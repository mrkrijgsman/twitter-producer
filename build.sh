#!/bin/bash

mvn clean install
cd docker

docker-compose build
docker-compose up