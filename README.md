### Start services:  
`./build.sh`

### Follow the Kafka topic:  
```
docker exec -it kafka /bin/bash
# In container:
$KAFKA_HOME/bin/kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic twitter
```

### Swagger
`http://localhost:8080/swagger-ui.html`