package nl.luminis.techday.twitter.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import nl.luminis.techday.twitter.rest.TwitterController;
import twitter4j.Status;
import twitter4j.StatusAdapter;

@Service
public class StreamListener extends StatusAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(TwitterController.class);
    private static final String KAFKA_TOPIC = "twitter";

    private final KafkaTemplate<String, String> template;

    public StreamListener(KafkaTemplate<String, String> template) {
        this.template = template;
    }

    @Override
    public void onStatus(Status status) {
        template.send(KAFKA_TOPIC, status.getText());
        LOGGER.info("{} : {}", status.getUser().getName(), status.getText());
    }

    @Override
    public void onException(Exception ex) {
        LOGGER.error("An error occurred in the Twitter stream", ex);
    }

}
