package nl.luminis.techday.twitter.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.annotations.ApiOperation;
import nl.luminis.techday.twitter.service.StreamListener;
import twitter4j.FilterQuery;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

@Controller
@RequestMapping("/twitter/stream")
public class TwitterController {

    private static final String DEFAULT_LANGUAGE_ENGLISH = "en";

    private TwitterStream stream;
    private final StreamListener streamListener;

    @Autowired
    public TwitterController(StreamListener streamListener,
                             @Value("${nl.luminis.techday.twitter.consumer.key}") String consumerKey,
                             @Value("${nl.luminis.techday.twitter.consumer.secret}") String consumerSecret,
                             @Value("${nl.luminis.techday.twitter.access.token}") String accessToken,
                             @Value("${nl.luminis.techday.twitter.access.token.secret}") String accessTokenSecret) {
        this.streamListener = streamListener;

        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true).setOAuthConsumerKey(consumerKey).setOAuthConsumerSecret(consumerSecret).setOAuthAccessToken(accessToken).setOAuthAccessTokenSecret(accessTokenSecret);
        stream = new TwitterStreamFactory(cb.build()).getInstance();
    }

    @GetMapping(value = "/start", produces = MediaType.TEXT_PLAIN_VALUE)
    @ApiOperation("Start streaming from Twitter towards Kafka")
    public String start(@RequestParam("keywords") String keywords, @RequestParam(value = "lang", required = false) String language) {
        FilterQuery filter = createFilter(keywords.split(" "), language);
        stream.addListener(streamListener);
        stream.filter(filter);

        return "Started new stream for keywords " + keywords;
    }

    @GetMapping(value = "/stop", produces = MediaType.TEXT_PLAIN_VALUE)
    @ApiOperation("Stop all currently active Twitter streams")
    public String stop() {
        stream.clearListeners();
        stream.shutdown();

        return "Stream has been cleared & shut down";
    }

    private FilterQuery createFilter(String[] queries, String language) {
        FilterQuery filter = new FilterQuery();
        filter.track(queries);
        if (language == null) {
            filter.language(DEFAULT_LANGUAGE_ENGLISH);
        } else {
            filter.language(language);
        }
        return filter;
    }
}
